const express = require('express');
const cors = require('cors');

const getFilesRouter = require('./controllers/routes/getFiles');
const getFileRouter = require('./controllers/routes/getFile');
const createFileRouter = require('./controllers/routes/createFile');
const errorMiddleware = require("./controllers/middleware/errorMiddleware");
const logger = require('./logger/index');

const app = express();

const PORT = 8080;

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: false}));

app.use('/', getFilesRouter);
app.use('/', getFileRouter);
app.use('/', createFileRouter);
app.use(errorMiddleware);

app.listen(PORT, () => {
    logger.info(`Server is running on port ${PORT}`)
})


