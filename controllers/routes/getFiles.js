const fs = require('fs');
const path = require('path');
const {Router} = require('express');
const logger = require('../../logger/index');

const router = Router();

router.get(`/api/files`, (req, res) => {
    fs.readdir(
        path.join(__dirname, '../..', 'files'),
        'utf-8',
        (err, files) => {
            if (err) {
                res.json({message: 'Error'});
            }
            logger.info(files.join(' '));
            res.type('application/json').end(JSON.stringify({message: "Success", files}, null, 2))
        }
    )
});

module.exports = router
