const fs = require('fs');
const path = require('path');
const {Router} = require('express');
const handleError = require("../../helper/handleError");
const logger = require('../../logger/index');

const router = Router();

router.get(`/api/files/:filename`, (req, res, next) => {
    try {
        const filename = req.params.filename;
        const filesFolder = path.join(__dirname, '../..', 'files/');

        fs.readFile(
            filesFolder + filename,
            'utf-8',
            (err, content) => {
                if (err) {
                    logger.error(err);
                    return res.status(400).json({message: 'Error'});
                }

                const uploadedDate = fs.statSync(filesFolder + filename).birthtime;
                const extension = path.extname(filename).slice(1);

                res.writeHead(200, {
                    'Content-Type': 'application/json'
                }).end(JSON.stringify({
                    message: "Success",
                    filename,
                    content,
                    extension,
                    uploadedDate
                }, null,  2))
                logger.info("Success")
            }
        )
    } catch (e) {
        next(e);
    }
});

module.exports = router
