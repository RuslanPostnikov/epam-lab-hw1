const fs = require('fs');
const path = require('path');
const {Router} = require('express');
const fileCreateValidator = require("../validator/file/fileCreateValidator");
const logger = require('../../logger/index');

const router = Router();

router.post(`/api/files`, fileCreateValidator, (req, res) => {
    if(req.isValid) {
        const filesFolder = path.join(__dirname, '../..', 'files');
        if(!fs.existsSync(filesFolder)) fs.mkdirSync(filesFolder);
        fs.writeFile(
            path.join(filesFolder, req.body.filename),
            req.body.content,
            function (err) {
                if (err) return res.json({message: 'Error'});
                console.log('Saved!');
                res.type('application/json').end(JSON.stringify({message: "File created successfully"}, null, 2));
                logger.info("File created successfully")
            }
        );
    } else {
        return res.status(400).json({message: `Please specify 'content' parameter`})
    }
});

module.exports = router;
