function fileCreateValidator(req, res, next) {
    try {
        const validExtension = /^.*\.(log|txt|json|yaml|xml|js)$/;
        const {filename, content } = req.body;
        req.isValid = validExtension.test(filename) && content.length;
        next();
    } catch (e) {
        next(e)
    }
}

module.exports = fileCreateValidator;
